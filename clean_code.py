#!/usr/bin/env python3
import argparse
import logging
import os
import shutil
import sys
import textwrap

import libs.backup as backup
import libs.core as core
import libs.cxx as cxx
import libs.report as report


def init_logging(verbose):
    logging_level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(format='%(message)s', stream=sys.stderr, level=logging_level)


def init_output_dir(output_dir, erase_output_dir):
    if erase_output_dir and os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir, exist_ok=True)
    backup.init_backup_dir(output_dir)


def check_unused_file(should_clean, report_file_path, origin_file_path, backup_file_path, build_command):
    logging.debug("Checking if file is unused...")
    if core.check_unused_file(origin_file_path, backup_file_path, build_command):
            logging.info("Unused file")
            report.report_unused_file(report_file_path, origin_file_path)
            if should_clean:
                os.remove(origin_file_path)
            return True
    return False


def check_unused_lines(should_clean, report_file_path, origin_file_path, backup_file_path, build_command, lines_to_check):
    logging.debug("Checking if file has unused lines: {}...".format(lines_to_check))
    unused_lines = core.check_unused_lines(origin_file_path, backup_file_path, build_command, lines_to_check)
    if len(unused_lines):
        logging.info("Unused lines: '{}'".format(unused_lines))
        report.report_unused_lines(report_file_path, origin_file_path, unused_lines)
        if should_clean:
            core.remove_lines(backup_file_path, origin_file_path, unused_lines)


def check_cxx_files(args, report_file_path):
    should_clean = not args.no_cleaning
    should_remove_unused_files = should_clean and args.remove_unused_files
    check_most_used_as_first = not args.check_most_used_as_last
    cxx_files = cxx.sort_cxx_files(cxx.find_cxx_files(args.repository_dir), check_most_used_as_first)
    cxx_files_len = len(cxx_files)

    for cxx_file_number, cxx_file_path in enumerate(cxx_files):
        backup_file_path = backup.get_backup_file_path(args.output_dir, cxx_file_path)

        logging.info("{}/{} Checking file: '{}'...".format(cxx_file_number + 1, cxx_files_len, cxx_file_path))
        backup.restore_file_if_needed(cxx_file_path, backup_file_path)
        backup.backup_file(cxx_file_path, backup_file_path)

        if check_unused_file(should_remove_unused_files, report_file_path, cxx_file_path, backup_file_path, args.build_command):
            backup.remove_backup(backup_file_path)
            continue

        lines_to_check = cxx.find_include_lines(cxx_file_path) + cxx.find_forward_decl_lines(cxx_file_path)
        check_unused_lines(should_clean, report_file_path, cxx_file_path, backup_file_path, args.build_command, lines_to_check) 
        
        backup.remove_backup(backup_file_path)


def main(args):
    init_logging(args.verbose)
    init_output_dir(args.output_dir, args.erase_output_dir)
    report_file_path = report.init_report(args.output_dir)

    logging.info("Prebuilding...")
    if not core.build(args.build_command):
        logging.error("Build failed.")
        return

    check_cxx_files(args, report_file_path)

    logging.info("Done.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=textwrap.dedent("""
        Tool for searching and removing of unused code.
        Removes code and verifies if C/C++ project compiles.
        """),
        formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("repository_dir", help="Path to the project repository")
    parser.add_argument("output_dir", help="Path to the folder with output results")
    parser.add_argument("build_command", help="Project build command")
    parser.add_argument("--no_cleaning", help="Only analizes")
    parser.add_argument("--remove_unused_files", help="Removes unused files")
    parser.add_argument("--erase_output_dir", help="Erase output bifore")
    parser.add_argument("--check_most_used_as_last", help="Check most used files as last")
    parser.add_argument("--verbose", help="Verbose logging")
    args = parser.parse_args()
    main(args)

