import json


def _encode_sets(obj):
    if isinstance(obj, set):
        return sorted(list(obj))
    raise TypeError(repr(obj) + " is not JSON serializable")


def save_json(filename, data):
    with open(filename, 'w') as json_file:
        json_file.write(json.dumps(data, indent=2, default=_encode_sets))


def open_json(filename):
    with open(filename) as json_file:
        return json.load(json_file)

