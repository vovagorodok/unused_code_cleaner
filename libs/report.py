import logging
import os

import libs.json_io as json_io


def open_report(report_file_path):
    return dict(json_io.open_json(report_file_path))
    

def init_report_file(report_file_path):
    if os.path.exists(report_file_path):
        logging.debug("Removing old report...")
        os.remove(report_file_path)
    json_io.save_json(report_file_path, dict())
    

def init_report(output_dir):
    report_file_path = os.path.join(output_dir, 'report.json')
    init_report_file(report_file_path)
    return report_file_path


def report_unused_file(report_file_path, origin_file_path):
    report = open_report(report_file_path)
    unused_files_list = report.get('unused files', list())
    unused_files_list.append(origin_file_path)
    report['unused files'] = unused_files_list
    json_io.save_json(report_file_path, report)


def _get_informative_lines(origin_file_path, numbers):
    informative_lines = list()
    lines = open(origin_file_path, 'r').readlines()
    for number, line in enumerate(lines):
        if number in numbers:
            informative_lines.append("{}: {}".format(number + 1, line.strip('\n')))
    return informative_lines


def report_unused_lines(report_file_path, origin_file_path, unused_lines):
    report = open_report(report_file_path)
    unused_lines_dict = report.get('unused lines', dict())
    unused_lines_dict[origin_file_path] = _get_informative_lines(origin_file_path, unused_lines)
    report['unused lines'] = unused_lines_dict
    json_io.save_json(report_file_path, report)

