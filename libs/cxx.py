import os
import re

import libs.core as core


def _find_local_includes(cxx_file_path):
    found_includes = list()
    lines = open(cxx_file_path, 'r').readlines()
    for line in lines:
        match = re.match(r'\#include.*\.(h|hpp|hxx).*', line)
        if match:
            with_brackets = re.search(r'(\"|\<)[^(\"|\<|\>)]+(\"|\>)', match.group(0))
            without_brackets = re.search(r'[^(\"|\<|\>)]+', with_brackets.group(0))
            without_path = os.path.basename(without_brackets.group(0))
            found_includes.append(without_path)
    return found_includes


def _create_most_use_includes_rating(cxx_files):
    rating = dict()
    for cxx_file_path in cxx_files:
        for local_include in _find_local_includes(cxx_file_path):
            rating[local_include] = rating.get(local_include, 0) + 1
    return rating


def sort_cxx_files(cxx_files, sort_most_used_first):
    rating = _create_most_use_includes_rating(cxx_files)
    most_used_first = sorted(cxx_files, key=lambda x: rating.get(os.path.basename(x), 0), reverse=sort_most_used_first)
    return most_used_first


def find_cxx_files(repository_dir):
    return core.find_files(repository_dir, r'.*\.(h|hpp|hxx|c|cpp|cxx)')


def find_include_lines(cxx_file_path):
    return core.find_lines(cxx_file_path, r'\#include.*')


def find_forward_decl_lines(cxx_file_path):
    return core.find_lines(cxx_file_path, r'(namespace *\{ *|)(struct|class|union|enum|class enum) +[^ ]+ *; *(\}|)')

