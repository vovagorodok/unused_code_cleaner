import logging
import os
import shutil


def init_backup_dir(output_dir):
    os.makedirs(os.path.join(output_dir, 'backup'), exist_ok=True)


def remove_backup(backup_file_path):
    os.remove(backup_file_path)


def get_backup_file_path(output_dir, origin_file_path):
    origin_file_name = os.path.basename(origin_file_path)
    return os.path.join(output_dir, 'backup', origin_file_name)


def backup_file(origin_file_path, backup_file_path):
    shutil.copyfile(origin_file_path, backup_file_path)


def restore_file(origin_file_path, backup_file_path):
    shutil.copyfile(backup_file_path, origin_file_path)


def restore_file_if_needed(origin_file_path, backup_file_path):
    if os.path.exists(backup_file_path):
        logging.info("Unexpected exit. Restoring...")
        restore_file(origin_file_path, backup_file_path)

