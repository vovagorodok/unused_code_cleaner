import logging
import os
import re
import subprocess
import sys

import libs.backup as backup


def _filter_strings(strings, regex):
    return list(filter(lambda string: re.fullmatch(regex, string), strings))


def find_files(repository_dir, file_regex):
    found_files = list()
    for root, directories, files in os.walk(repository_dir):
        for file in _filter_strings(files, file_regex):
            found_files.append(os.path.join(root, file))
    return found_files


def find_lines(file, line_regex):
    found_lines = list()
    lines = open(file, 'r').readlines()
    for number, line in enumerate(lines):
        if re.match(line_regex, line):
            found_lines.append(number)
    return found_lines


def remove_line(input_file, output_file, number):
    os.remove(output_file)
    infile = open(input_file, 'r').readlines()
    with open(output_file, 'w') as outfile:
        for index, line in enumerate(infile):
            if index != number:
                outfile.write(line)


def remove_lines(input_file, output_file, numbers):
    os.remove(output_file)
    infile = open(input_file, 'r').readlines()
    with open(output_file, 'w') as outfile:
        for index, line in enumerate(infile):
            if index not in numbers:
                outfile.write(line)


def _run_cmd(cmd):
    stdout_stream = sys.stdout if logging.root.isEnabledFor(logging.DEBUG) else subprocess.DEVNULL
    try:
        return subprocess.check_call(cmd, stdin=subprocess.PIPE, stdout=stdout_stream, stderr=stdout_stream, shell=True, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        return e.returncode


def build(build_command):
    if _run_cmd(build_command):
        logging.debug("Build failed")
        return False
    else:
        logging.debug("Build success")
        return True


def check_unused_file(origin_file_path, backup_file_path, build_command):
    os.remove(origin_file_path)
    is_file_unused = build(build_command)

    backup.restore_file(origin_file_path, backup_file_path)
    return is_file_unused


def check_unused_lines(origin_file_path, backup_file_path, build_command, lines_to_check):
    unused_lines = list()  

    for line_number in lines_to_check:
        logging.debug("Checking line: {}...".format(line_number))
        currently_unused_lines = [line_number] + unused_lines
        remove_lines(backup_file_path, origin_file_path, currently_unused_lines)

        if build(build_command):
            unused_lines.append(line_number)

    backup.restore_file(origin_file_path, backup_file_path)
    return unused_lines

