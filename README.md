# UNUSED CODE CLEANER
**[REMARK] Removing of unused includes or forward decl just without knowelage of code base is not possible.**
**Only trully static analysis can do that.**
**Problem is in case whan we should decide if include or forward decl should be added if it was added at already added include.**
**I decide to leave this code just as interesting proof of concept.**
**Use include-what-you-use or any other tool for that.**
**Or am I wrong ?**

Helps to remove code unnecessary for successful compilation.

Simple way to clean some cmake project:
```shell
mkdir ~/some_cmake_project
cd ~/some_cmake_project

git clone https://some_cmake_project.git src

mkdir build
cd build

cmake ../src
./clean_code.py ~/some_cmake_project/src ~/some_cmake_project/out "cd ~/some_cmake_project/build && make"
```

Where:
- repository_dir: `~/some_cmake_project/src`
- output_dir: `~/some_cmake_project/out`
- build_command: `"cd ~/some_cmake_project/build && make"`

To check results:
```shell
cat ~/some_cmake_project/out/report.json
```